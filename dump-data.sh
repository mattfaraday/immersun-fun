#!/bin/bash

# Written by: Matt Faraday <matt@faraday.at>

# This is an experiment to extract the data from myimmersun and store it in a database (memcache)
# You need an immersun unit with a myimmersun bridge already setup
# You also need your account details for the myimmersun website.

# Put website account details here:

USERID=putyouruseridhere
PASSWORD=putyourpasswordhere


# You NEED to have memcache installed first or you need to comment out all the memcache bits
# On ubuntu systems run the commend "apt-get install memcached"


### BEGIN SCRIPT. REMEMBER, NO WARRANTIES :D

# First check for old loose end files and remove

if [ -f index.html ];
then
        rm index.html*
fi

wget -q http://live.myimmersun.com --user=$USERID --password=$PASSWORD

CURCONSUMP=`cat index.html |  grep -i orange | awk '{print $3;}' | cut -d W -f 1 | cut -c4-`
TABSGREEN=`cat index.html | grep -i green | awk '{print $3;}' | cut -d W -f 1 | cut -c4-`
SOLARPV=`cat index.html |  grep -i yellow | awk '{print $6;}' | cut -d W -f 1 | cut -c4-`
DIVERT=`cat index.html | grep blue | awk '{print $6;}' | cut -dW -f1 | cut -c 4- | sed -E 's/<[^>]*>//g'`


# Old inefficient way of making 4 requests to the webserver. Now we make 1 and parse it 4 times. 

#CURCONSUMP=`wget -qO- http://live.myimmersun.com --user=$USERID --password=$PASSWORD | grep -i orange | awk '{print $3;}' | cut -d W -f 1 | cut -c4-`
#TABSGREEN=`wget -qO- http://live.myimmersun.com --user=$USERID --password=$PASSWORD | grep -i green | awk '{print $3;}' | cut -d W -f 1 | cut -c4-`
#SOLARPV=`wget -qO- http://live.myimmersun.com --user=$USERID --password=$PASSWORD | grep -i yellow | awk '{print $6;}' | cut -d W -f 1 | cut -c4-`
#DIVERT=`wget -qO- http://live.myimmersun.com --user=$USERID --password=$PASSWORD | grep blue | awk '{print $6;}' | cut -dW -f1 | cut -c 4- | sed -E 's/<[^>]*>//g'`

# An example if you wanted to just echo the result of the wget commands

#echo "Current consumption: $CURCONSUMP W"
#echo "Current solar PV: $SOLARPV W"
#echo "Current export: $TABSGREEN W" 

# Save data in memcache

echo -e "set i_cons 0 30 ${#CURCONSUMP}\r\n$CURCONSUMP\r"| nc localhost 11211 | grep -v "^STORED"
echo -e "set i_pv 0 30 ${#SOLARPV}\r\n$SOLARPV\r"|nc localhost 11211 | grep -v "^STORED"
echo -e "set i_exp 0 30 ${#TABSGREEN}\r\n$TABSGREEN\r"|nc localhost 11211 | grep -v "^STORED" 
echo -e "set i_div 0 30 ${#DIVERT}\r\n$DIVERT\r" | nc localhost 11211 | grep -v "^STORED"

# retrieve data from memcache

echo -n "Current consumption: "
echo -e "get i_cons\r" | nc localhost 11211  | grep -v "^VALUE" | grep -v "^END"
echo -n "Current solar production: " 
echo -e "get i_pv\r" | nc localhost 11211  | grep -v "^VALUE" | grep -v "^END"
echo -n "Current export: " 
echo -e "get i_exp\r" | nc localhost 11211  | grep -v "^VALUE" | grep -v "^END"
echo -n "Current divert: " 
echo -e "get i_div\r" | nc localhost 11211 | grep -v "^VALUE" | grep -v "^END"

# This section checks to see if there is any export 
# If there is an export, it converts it to an integer and compares it to different values, giving a different message

ISINT=true


# If the variable $TABSGREEN is empty, then all the arithmetic tests will give errors, so check if its empty before doing calculations

if [[ $TABSGREEN == "" ]]; then
	echo "No power being exported"
	ISINT=false
fi

# We have determined the number is a real number
# Cast it to an integer first just to ensure no problems manipulating integers

if [[ $ISINT == "true" ]]; then
	result=$( echo "$TABSGREEN +0" | bc)

# This section of code only gets executed if an export is taking place and its greater than 0W
elif [[ $result > 500 ]]; then
	echo "exporting more than 500W"

elif [[ $result > 600 ]]; then
	echo "exporting more than 600W"

elif [[ $result > 1000 ]]; then
	echo "exporting more than 1kW"
fi


#Tidy up any wget loose ends..

if [ -f index.html ];
then
	rm index.html*
fi



## EXIT THE SCRIPT WITH SUCCESS
exit 0
