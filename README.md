## This repo will contain scripts/tools/dirty hacks to control or talk to an immersun unit via the myimmersun website.



THERE IS CURRENTLY A KNOWN ISSUE WITH WGET VERSION <1.17 WILL GIVE A 401 ERROR ... SEE https://gitlab.com/mattfaraday/immersun-fun/issues/1 or upgrade wget. 



dump-data.sh is an example bash script that will dump the html data from the myimmersun website and save it on the disk. 
it will then parse the html and store each of the data values in a seperate variable. It then stores the content of those variables in memcache. 
the downloaded html file is then deleted. 

This way, we have seaparated out all of the values and stored them in a database and we can do whatever we like with those values. 

You could crontab the script to run at whatever interval you like. 

You could use the bash script to determine if you should turn on a device. For example, if you are making excess power and exporting a lot of power then you may want to turn
on a device. Like a heater, or kettle etc. The immersun only allows resistive loads. But by using a simple rasberry pi + relay module you can turn any device on or off. 

The idea is to build clients that pull their data from the memcache running on your "server". 

This way, the server requests and processes the data once, then makes it available via memcache and updates it regularly. 
Therefore each device does not have to continue to poll the website for data. It can just pull the data from memcache over the network. 

Using the data provided you could:


Display all the data. Power exported, power consumed, power diverted.

Build a display to show the information

Build an automatic switch to turn on a device when excess power is being exported (e.g. a battery charger) 

Make statistical graphs of the data

etc

Please email any questions/comments to the author.<matt@faraday.at>